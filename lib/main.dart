// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import './providers/products.dart';
// import './screens/product_detail_screen.dart';
// import './screens/products_overview_screen.dart';

// void main() => runApp(const MyApp());

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final ThemeData theme = ThemeData(fontFamily: "Lato");
//     //> when ctx is used - (here the best way is to not use value!)
//     //value is used on existing objects -> here we have a new instance...
//     return ChangeNotifierProvider(
//       create: (ctx) => Products(),
//       //> when ctx is not used
//       // return ChangeNotifierProvider.value(
//       //   value: Products(),
//       child: MaterialApp(
//           title: 'MyShop',
//           theme: theme.copyWith(
//             colorScheme: theme.colorScheme
//                 .copyWith(primary: Colors.purple, secondary: Colors.deepOrange),
//           ),
//           home: const ProdutsOverviewScreen(),
//           routes: {
//             ProductDetailScreen.routeName: (ctx) => const ProductDetailScreen()
//           }),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './screens/splash_screen.dart';
import './screens/products_overview_screen.dart';
import './screens/auth_screen.dart';
import './screens/edit_product_screen.dart';
import './screens/user_products_screen.dart';
import './screens/product_detail_screen.dart';
import './screens/cart_screen.dart';
import './screens/orders_screen.dart';
import './providers/products.dart';
import './providers/cart.dart';
import './providers/orders.dart';
import './providers/auth.dart';
import './helpers/custom_route.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData(fontFamily: "Lato");
    return MultiProvider(
      providers: [
        // ChangeNotifierProvider.value(
        //   value: Products(),
        // ),
        // ChangeNotifierProvider.value(
        //   value: Cart(),
        // ),
        //best pratice here
        //for list its the .value approach
        ChangeNotifierProvider(
          create: (ctx) => Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, Products>(
          create: (ctx) => Products(null, null, []),
          update: (ctx, auth, previousProducts) => Products(
              auth.token,
              auth.userId,
              previousProducts == null ? [] : previousProducts.items),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),
        ),
        ChangeNotifierProxyProvider<Auth, Orders>(
          create: (ctx) => Orders(null, null, []),
          update: (ctx, auth, previousOrders) => Orders(auth.token, auth.userId,
              previousOrders == null ? [] : previousOrders.orders),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
            title: 'MyShop',
            theme: theme.copyWith(
                colorScheme: theme.colorScheme.copyWith(
                    primary: Colors.purple, secondary: Colors.deepOrange),
                pageTransitionsTheme: PageTransitionsTheme(
                  builders: {
                    TargetPlatform.android: CustomPageTransictionBuilder(),
                    TargetPlatform.iOS: CustomPageTransictionBuilder(),
                  },
                )),

            // theme: ThemeData(
            //   fontFamily: 'Lato',
            //   colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.purple)
            //       .copyWith(secondary: Colors.deepOrange),
            // ),
            home: auth.isAuth
                ? const ProductsOverviewScreen()
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (
                      ctx,
                      authResultSnapshot,
                    ) =>
                        authResultSnapshot.connectionState ==
                                ConnectionState.waiting
                            ? const SplashScreen()
                            : const AuthScreen(),
                  ),
            routes: {
              ProductDetailScreen.routeName: (ctx) =>
                  const ProductDetailScreen(),
              CartScreen.routeName: (ctx) => const CartScreen(),
              OrdersScreen.routeName: (ctx) => const OrdersScreen(),
              UserProductsScreen.routeName: (ctx) => const UserProductsScreen(),
              EditProductScreen.routeName: (ctx) => const EditProductScreen(),
            }),
      ),
    );
  }
}
